<?php

/**
 * @file
 * Theme functions for the queue_pager module.
 */

/**
 * Returns HTML for the queue pager next link.
 *
 * @param array $variables
 *   An associative array containing:
 *   - node: The next node
 *   - queue: The queue to which it belongs.
 */
function theme_queue_pager_item($variables) {
  $link_text = $variables['link_text'];
  $link_type = $variables['link_type'];
  $link_href = $variables['link_href'];

  $attributes = array('class' => array('queue-pager', 'queue-pager--' . $link_type));
  $link_options = array(
    'attributes' => array(
      'class' => array('queue-pager__link', 'queue-pager__link--' . $link_type),
    ),
  );

  $ret = theme('queue_pager_link', array(
    'link_text' => $link_text,
    'attributes' => $attributes,
    'link_options' => $link_options,
    'link_href' => $link_href,
  ));

  return $ret;
}

/**
 * Returns HTML for the queue pager next link.
 *
 * @param array $variables
 *   An associative array containing:
 *   - node: The next node
 *   - queue: The queue to which it belongs.
 */
function theme_queue_pager_next($variables) {
  $variables['link_type'] = 'next';
  return (theme('queue_pager_item', $variables));
}

/**
 * Returns HTML for the queue pager previous link.
 *
 * @param array $variables
 *   An associative array containing:
 *   - node: The previous node
 *   - queue: The queue to which it belongs.
 */
function theme_queue_pager_prev($variables) {
  $variables['link_type'] = 'prev';
  return (theme('queue_pager_item', $variables));
}

/**
 * Returns HTML for a queue pager link.
 *
 * @param array $variables
 *   An associative array containing:
 *   - link_text: The text for the link
 *   - attributes: The attributes for the div wrapper
 *   - link_options: Any options for the link
 *   - link_href: The link destination.
 */
function theme_queue_pager_link($variables) {
  $link_text = $variables['link_text'];
  $attributes = $variables['attributes'];
  $link_options = $variables['link_options'];
  $link_href = $variables['link_href'];

  $ret = sprintf('<div %s>%s</div>', drupal_attributes($attributes), l($link_text, $link_href, $link_options));
  return $ret;
}
