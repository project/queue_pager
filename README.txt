INTRODUCTION
------------

Queue Pager enables you to add previous and next links to nodes (or other
entities) that are part of a nodequeue or entityqueue, which will allow you to
navigate through the queue from page to page. Queue Pager can be enabled per
queue, and the presentation of the pager links can be managed from every view
mode of entity types that are available to that queue.

Use this module if you want to be able to navigate sequentially between a 
pre-defined queue of entities.

Project page:
https://www.drupal.org/project/queue_pager


REQUIREMENTS
------------

This module requires either the nodequeue or entityqueue module:
https://drupal.org/project/nodequeue
https://drupal.org/project/entityqueue


CONFIGURATION
-------------

To enable the queue pager for specific queues, go to either the nodequeue or
entityqueue's configuration page and check 'Enable queue pager for this
nodequeue/entityqueue.'

After that you may override the position and link text for the queues on the 
'Manage Display' pages for all entity types that are valid types for that queue.

Note that queue pager currently only works for simple queues (ie queues with
only one subqueue).


TROUBLESHOOTING
---------------

If you notice any problems or experience any issues, please raise an issue on 
the project's issue queue:
https://www.drupal.org/project/issues/queue_pager


MAINTAINERS
-----------

Current maintainers:
 * Soames Brockelbank (somatick) - https://drupal.org/user/345496

This project has been sponsored by:
 * Sparks Interactive
   http://www.sparksinteractive.co.nz/
sset